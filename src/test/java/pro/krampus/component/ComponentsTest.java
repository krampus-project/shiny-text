package pro.krampus.component;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComponentsTest {
    @Test
    void empty() {
        assertEquals(new ComponentBuilder("").create()[0], Components.empty()[0]);
    }

    @Test
    void fromLegacy() {
        // Expected value that is constructed with ComponentBuilder
        BaseComponent[] builderConstructed = new ComponentBuilder("A test string").bold(true)
                .italic(true).strikethrough(true).underlined(true).obfuscated(true).color(ChatColor.BLACK).create();

        // The actual value that is returned by library method(s)
        BaseComponent[] legacyConverted = Components.fromLegacy(
                ChatColor.translateAlternateColorCodes('&', "&0&l&o&m&n&kA test string"));
        assertEquals(builderConstructed[0], legacyConverted[0]);
    }

    @Test
    void fromJson() {
        BaseComponent[] builderConstructed = new ComponentBuilder("A test string").bold(true)
                .italic(true).strikethrough(true).underlined(true).obfuscated(true).color(ChatColor.BLACK).create();
        BaseComponent[] jsonDeserialized = Components.fromJson("{\"text\":\"A test string\",\"bold\":true," +
                "\"italic\":true,\"strikethrough\":true,\"underlined\":true,\"obfuscated\":true,\"color\":\"black\"}");
        assertEquals(builderConstructed[0], jsonDeserialized[0]);
    }

    @Test
    void fromMarkdown() {
        // Notice: Markdown does not support obfuscation and coloring; not adding those attributes in this test
        BaseComponent[] builderConstructed = new ComponentBuilder("A test string").bold(true)
                .italic(true).strikethrough(true).underlined(true).append(" w/ substring").reset().create();
        BaseComponent[] markdownConverted = Components.fromMarkdown("***__~~A test string~~__*** w/ substring");
        assertEquals(builderConstructed.length, markdownConverted.length);
        for (int i = 0; i < builderConstructed.length; i++) {
            assertEquals(builderConstructed[i], markdownConverted[i]);
        }
    }
}