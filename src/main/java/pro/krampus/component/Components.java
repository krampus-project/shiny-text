package pro.krampus.component;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.commonmark.ext.gfm.strikethrough.StrikethroughExtension;
import org.commonmark.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Utility class for creating and manipulating component arrays.
 */
public final class Components {
    private static final String EMPTY_STRING = "";
    private static final Parser MD_PARSER = new Parser.Builder().extensions(
            Collections.singletonList(StrikethroughExtension.create())
    ).build();
    private static final MarkdownComponentRenderer MD_RENDERER = new MarkdownComponentRenderer();

    private Components() {

    }

    /**
     * Build an empty component array.
     *
     * @return an empty {@link BaseComponent} array.
     */
    @NotNull
    public static BaseComponent[] empty() {
        return TextComponent.fromLegacyText(EMPTY_STRING);
    }

    /**
     * Build a component array from a plain text.
     *
     * @return array size of 1 of {@link BaseComponent} of given plain text.
     */
    @NotNull
    public static BaseComponent[] fromPlainText(@NotNull String text) {
        return new ComponentBuilder(text).create();
    }

    /**
     * Build a component array from the legacy formatted string.
     *
     * @param legacy {@link String} of legacy formatting.
     * @return Array of {@link BaseComponent}s built based on the legacy formatting.
     */
    @NotNull
    public static BaseComponent[] fromLegacy(@NotNull String legacy) {
        return TextComponent.fromLegacyText(legacy);
    }

    /**
     * Parse JSON text into a {@link BaseComponent} array.
     *
     * @param json JSON {@link String} to parse.
     * @return Array of {@link BaseComponent}s built by parsing JSON.
     */
    @NotNull
    public static BaseComponent[] fromJson(@NotNull String json) {
        return ComponentSerializer.parse(json);
    }

    /**
     * Parse Markdown into a {@link BaseComponent} array.
     *
     * @param markdown Markdown {@link String} to parse.
     * @return Array of {@link BaseComponent}s built by parsing Markdown.
     */
    @NotNull
    public static BaseComponent[] fromMarkdown(@NotNull String markdown) {
        return MD_RENDERER.renderComponents(MD_PARSER.parse(markdown));
    }

    public static TextComponent substitute(@NotNull TextComponent component,
                                           @NotNull Map<String, BaseComponent[]> substitutions,
                                           char delimiter) {
        TextComponent newComponent = new TextComponent(component);
        String text = newComponent.getText();
        List<BaseComponent> extraComponents = newComponent.getExtra();
        if (extraComponents == null) {
            extraComponents = new ArrayList<>();
        }

        for (int i = 0; i < extraComponents.size(); i++) {
            BaseComponent extraComponent = extraComponents.get(i);
            if (extraComponent instanceof TextComponent) {
                extraComponents.set(i, substitute(((TextComponent) extraComponent), substitutions, delimiter));
            }
        }

        StringBuilder textBuilder = new StringBuilder();
        StringBuilder labelBuilder = new StringBuilder();
        StringBuilder extraBuilder = new StringBuilder();
        boolean escape = false;
        boolean inLabel = false;
        int insertIndex = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (!escape) {
                if (c == '\\') {
                    escape = true;
                    continue;
                } else if (c == delimiter) {
                    if (inLabel = !inLabel) {
                        // false -> true
                        // Entered label
                        extraComponents.add(insertIndex++, new TextComponent(textBuilder.toString()));
                        textBuilder.setLength(0);
                    } else {
                        // true -> false
                        // Exited label
                        String label = labelBuilder.toString();
                        for (Map.Entry<String, BaseComponent[]> substitutionEntry : substitutions.entrySet()) {
                            if (substitutionEntry.getKey().equals(label)) {
                                extraComponents.addAll(insertIndex++, Arrays.asList(substitutionEntry.getValue()));
                            }
                        }
                        labelBuilder.setLength(0);
                    }
                    continue;
                }
            }

            if (inLabel) {
                labelBuilder.append(c);
            } else {
                textBuilder.append(c);
            }

            escape = false;
        }

        if (textBuilder.length() > 0) {
            extraComponents.add(insertIndex++, new TextComponent(textBuilder.toString()));
        }

        newComponent.setText("");
        newComponent.setExtra(extraComponents);
        return newComponent;
    }


    /**
     * Substitute delimiter-surrounded literals in a provided {@link BaseComponent}s array.
     *
     * @param componentArray {@link BaseComponent}s array to substitute in.
     * @param substitutions  Label to {@link BaseComponent}s array map of substitutions.
     * @param delimiter      Substitution literal delimiter.
     * @return {@link BaseComponent}s array with substitutions applied.
     */
    @NotNull
    public static BaseComponent[] substitute(@NotNull BaseComponent[] components,
                                             @NotNull Map<String, BaseComponent[]> substitutions,
                                             char delimiter) {
        BaseComponent[] copy = new BaseComponent[components.length];
        for (int i = 0; i < components.length; i++) {
            BaseComponent component = components[i];
            if (component instanceof TextComponent) {
                copy[i] = substitute(((TextComponent) component), substitutions, delimiter);
            }
        }
        return copy;
    }
}
