package pro.krampus.component;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.chat.ComponentSerializer;
import org.commonmark.ext.gfm.strikethrough.Strikethrough;
import org.commonmark.node.*;
import org.commonmark.renderer.Renderer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public final class MarkdownComponentRenderer implements Renderer {
    private static class MarkdownVisitor extends AbstractVisitor {
        private final Set<Node> nodeSet = new HashSet<>();
        private ComponentBuilder builder;

        @Override
        public void visit(Emphasis emphasis) {
            this.nodeSet.add(emphasis);
            visitChildren(emphasis);
            this.nodeSet.remove(emphasis);
        }

        @Override
        public void visit(StrongEmphasis strongEmphasis) {
            this.nodeSet.add(strongEmphasis);
            visitChildren(strongEmphasis);
            this.nodeSet.remove(strongEmphasis);
        }

        @Override
        public void visit(CustomNode customNode) {
            this.nodeSet.add(customNode);
            visitChildren(customNode);
            this.nodeSet.remove(customNode);
        }

        @Override
        public void visit(Text text) {
            visitChildren(text);

            if (this.builder == null) {
                this.builder = new ComponentBuilder(text.getLiteral());
            } else {
                this.builder.append(text.getLiteral()).reset();
            }

            for (Node node : nodeSet) {
                if (node instanceof Emphasis) {
                    switch (((Emphasis) node).getOpeningDelimiter()) {
                        case "*":
                        case "_":
                            this.builder.italic(true);
                            break;
                    }
                }

                if (node instanceof StrongEmphasis) {
                    switch (((StrongEmphasis) node).getOpeningDelimiter()) {
                        case "**":
                            this.builder.bold(true);
                            break;
                        case "__":
                            this.builder.underlined(true);
                            break;
                    }
                }

                if (node instanceof CustomNode) {
                    if (node instanceof Strikethrough) {
                        this.builder.strikethrough(true);
                    }
                }
            }
        }

        public ComponentBuilder getBuilder() {
            return builder;
        }
    }

    /**
     * Render Markdown into an array of {@link BaseComponent}s.
     *
     * @param node Markdown {@link Node}.
     * @return array of {@link BaseComponent}s parsed from Markdown.
     */
    @NotNull
    public BaseComponent[] renderComponents(@NotNull Node node) {
        MarkdownVisitor visitor = new MarkdownVisitor();
        node.accept(visitor);
        return visitor.getBuilder().create();
    }

    /**
     * Render Markdown into a JSON-serialized array of {@link BaseComponent}s and write it to
     * an {@link Appendable} stream.
     *
     * @param node Markdown {@link Node}.
     */
    @Override
    public void render(@NotNull Node node, @NotNull Appendable output) {
        try {
            output.append(render(node));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Render Markdown into a JSON-serialized array of {@link BaseComponent}s.
     *
     * @param node Markdown {@link Node}.
     * @return JSON-serialized array of {@link BaseComponent}s parsed from Markdown.
     */
    @Override
    @NotNull
    public String render(@NotNull Node node) {
        return ComponentSerializer.toString(renderComponents(node));
    }
}
